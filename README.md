<!--
Source: https://stackoverflow.com/questions/43126475/pylint-badge-in-gitlab

Template:
[![pylint](https://gitlab.com/$GROUP/$SUBGROUP/$PROJECT_NAME/-/jobs/artifacts/$BRANCH/raw/public/badges/pylint.svg?job=pylint)](https://gitlab.com/$GROUP/$SUBGROUP/$PROJECT_NAME/-/jobs/artifacts/$BRANCH/browse/public/lint?job=pylint)
-->

[![pylint](https://gitlab.com/notantony/dummy-project/-/jobs/artifacts/dev/raw/public/badges/pylint.svg?job=pylint)](https://gitlab.com/notantony/dummy-project/-/jobs/artifacts/dev/browse/public/lint?job=pylint)


[![pytest](https://gitlab.com/notantony/dummy-project/-/jobs/artifacts/dev/raw/public/badges/pytest.svg?job=pytest)](https://gitlab.com/notantony/dummy-project/-/jobs/artifacts/dev/browse/public/badges?job=pytest)
