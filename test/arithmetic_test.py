from arithmetic import add, sub, mul


def test_add():
    assert add(3, 4) == 7

def test_sub():
    assert sub(5, 4) == 1

def test_mul():
    assert mul(3, 4) == 12
