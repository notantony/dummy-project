from typing import Union

Numeric = Union[float, int]

def add(x: Numeric, y: Numeric):
    return x + y

def sub(x: Numeric, y: Numeric):
    return x - y

def mul(x: Numeric, y: Numeric):
    return x * y
